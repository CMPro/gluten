library(gluten)

paths <- dir('input', '*.csv', full.names = TRUE)
names(paths) <- dir('input', '*.csv')

test_that('named is a boolean', {
  expect_error(dir_importer(paths, named = 10),
               'you are not logical')
})
